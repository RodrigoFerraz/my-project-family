const containerPai = document.getElementById("container-pai") 

function criarContainerTorres (f) { 
  let containerpaiTorres = document.createElement("div") ; 
  containerpaiTorres.classList = "container-towers"  
  containerPai.appendChild(containerpaiTorres)

  for(let i=1;i<=3;i++){ 
   let containerTorres = document.createElement("div") ; 
   containerTorres.classList = "tower" + i; 
  containerpaiTorres.appendChild(containerTorres) 
  }     
} 
criarContainerTorres() 

function criarDiscos (f) {
let divTower1 = document.getElementsByClassName("tower1") 
  for(let i=1;i<=4;i++){ 
   let divDiscos= document.createElement("div") ; 
   divDiscos.className ="disc"+ i;  
   divTower1[0].appendChild(divDiscos) 
  }   
} 
criarDiscos() 

// FUNCAO HANDLE DE CLIQUE 
const handleClickOne = (ev) => {      
let lastDisc = ev.target.lastElementChild   // movimenta o ultimo  disco 
return lastDisc
}  

const handleClickTwo = (ev,disc) => {           // elemento clicado  
  console.log(disc)
  ev.target.appendChild(disc)
}  

let disc = null 

const handleClick =(ev) => {
  let div = document.getElementsByClassName(ev.target.className)[0]
   if(disc === null ) {
     disc = ev.target.lastElementChild
   }
   if(div.lastChild === null){
   div.appendChild(disc)
   disc = null
   }
   if(div.lastChild.clientWidth > disc.clientWidth){
   div.appendChild(disc)
   disc = null
 } 
 console.log(disc)
 }
 

const tower1 = document.querySelector('.tower1')
tower1.addEventListener("click",handleClick)
const tower2 = document.querySelector('.tower2')
tower2.addEventListener("click",handleClick)
const tower3 = document.querySelector('.tower3')
tower3.addEventListener("click",handleClick)
